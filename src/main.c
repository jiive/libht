#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hash_table.h"

static size_t count_digits(int number);

int main() {
    char *keys[] = {"one", "two", "three", "four", "five"};
    char *found;
    time_t t;
    int i = 0;

    srand((unsigned) time(&t));

    ht_hash_table* ht = ht_new();
    for (i=0; i<5; i++) {
        const char *fmt = "%d";
        int number = rand();
        char *tmp = calloc(1, count_digits(number)+1);
        snprintf(tmp, 12, fmt, number);
        printf("Storing object \"%s\" in key \"%s\"\n", tmp, keys[i]);
        ht_insert(ht, keys[i], tmp);
    }


    for (i=0; i<5; i++) {
        found = ht_search(ht, keys[i]);
        if (found)
            printf("Found key %s with value %s\n", keys[i], found);
    }

    ht_del_hash_table(ht);
}

static size_t count_digits(int number) {
    size_t counter = 1;
    while ((number /= 10) > 10)
        counter++;
    return ++counter;
}
