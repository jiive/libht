/* Copyright (C) Joakim Vinteräng 2020
 * https://gitlab.com/jovii/libht
 * License: GPLv3+, see LICENSE file for details */

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "libht.h"

/* Static function declarations */
static size_t           ht_hash(const char *str, size_t prime, size_t max);
static size_t           ht_get_hash(const char *str, size_t num_buckets,
                                    size_t attempt);
static void             ht_del_item(ht_item *i);
static ht_item*         ht_new_item(const char *k, void *v);
static ht_hash_table*   ht_new_sized(const size_t base_size);
static void             ht_resize(ht_hash_table *ht, size_t base_size);
static void             ht_resize_up(ht_hash_table *ht);
static void             ht_resize_down(ht_hash_table *ht);
static size_t           is_prime(const size_t x);
static size_t           next_prime(size_t x);

static ht_item HT_DELETED_ITEM = {NULL, NULL};
static size_t HT_INITIAL_BASE_SIZE = 53;

/**
 * @brief Create new empty hashtable
 * @return Pointer to newly created hashtable
 */
ht_hash_table* ht_new() {
    return ht_new_sized(HT_INITIAL_BASE_SIZE);
}

/**
 * @brief Delete hashtable
 * @param ht Hashtable to delete
 */
void ht_del_hash_table(ht_hash_table* ht) {
    for (size_t i = 0; i < ht->size; i++) {
        ht_item* item = ht->items[i];
        if (item != NULL) {
            ht_del_item(item);
        }
    }

    free(ht->items);
    free(ht);
}

/**
 * @brief Insert pointer to object to be stored in hashtable
 * @param ht: hashtable
 * @param key: key for the stored object
 * @param object: object pointer to be stored
 */
void ht_insert(ht_hash_table* ht, const char* key, void* value) {
    size_t load = ht->count * 100 / ht->size;
    if (load > 70)
        ht_resize_up(ht);

    ht_item *item = ht_new_item(key, value);
    ht_item *curr_item = NULL;
    size_t i = 0;
    size_t index = 0;

    do {
        index = ht_get_hash(item->key, ht->size, i);
        curr_item = ht->items[index];
        if (curr_item != NULL && curr_item != &HT_DELETED_ITEM) {
            if (strcmp(curr_item->key, key) == 0) {
                /* Replace existing key */
                ht_del_item(curr_item);
                ht->items[index] = item;
                return;
            }
        }
        i++;
    } while (curr_item != NULL && curr_item != &HT_DELETED_ITEM);

    ht->items[index] = item;
    ht->count++;
}

/**
 * @brief Search hashtable for a specific key
 * @param ht: hashtable
 * @param key: string key for sought object
 * @return Pointer to found object, else NULL
 */
void* ht_search(ht_hash_table *ht, const char *key) {
    size_t index;
    ht_item *item;
    size_t i = 0;

    do {
        index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
        if (item != &HT_DELETED_ITEM) {
            if (strcmp(item->key, key) == 0) {
                return item->value;
            }
        }
        i++;
    } while (item != NULL);

    return NULL;
}

/**
 * @brief Delete object relating to key
 * @param ht: hashtable
 * @param key: string to for object to delete
 */
void ht_delete(ht_hash_table* ht, const char* key) {
    size_t load = ht->count * 100 / ht->size;
    if (load < 10)
        ht_resize_down(ht);

    size_t i = 0;
    ht_item *item = NULL;
    do {
        size_t index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];

        if (item != NULL && item != &HT_DELETED_ITEM) {
            if (strcmp(item->key, key) == 0) {
                ht_del_item(item);
                ht->items[index] = &HT_DELETED_ITEM;
                ht->count--;
                break;
            }
        }
        i++;
    } while (item != NULL);
}

/*****************************************************************************
 * Static functions
 ****************************************************************************/

static void ht_del_item(ht_item* i) {
    free(i->key);
    /* free(i->value); */
    free(i);
}

static ht_item* ht_new_item(const char *k, void *v) {
    ht_item* i = calloc(1, sizeof(ht_item));

    i->key = strdup(k);
    i->value = v;

    return i;
}

static ht_hash_table* ht_new_sized(const size_t base_size) {
    ht_hash_table *ht = calloc(1, sizeof(ht_hash_table));
    ht->base_size = base_size;

    ht->size = next_prime(ht->base_size);

    ht->count = 0;
    ht->items = calloc(ht->size, sizeof(ht_item*));

    return ht;
}


static void ht_resize(ht_hash_table *ht, size_t base_size) {
    if (base_size < HT_INITIAL_BASE_SIZE) return;

    ht_hash_table *new_ht = ht_new_sized(base_size);
    for (size_t i = 0; i < ht->size; i++) {
        ht_item *item = ht->items[i];
        if (item != NULL && item != &HT_DELETED_ITEM)
            ht_insert(new_ht, item->key, item->value);
    }

    ht->base_size = new_ht->base_size;
    ht->count = new_ht->count;

    /* Swap sizes */
    size_t tmp_size = ht->size;
    ht->size = new_ht->size;
    new_ht->size = tmp_size;

    /* Swap items */
    ht_item **tmp_items = ht->items;
    ht->items = new_ht->items;
    new_ht->items = tmp_items;

    /* new_ht contains old items now */
    ht_del_hash_table(new_ht);
}

static void ht_resize_up(ht_hash_table *ht) {
    size_t new_size = ht->base_size * 2;
    ht_resize(ht, new_size);
}

static void ht_resize_down(ht_hash_table *ht) {
    size_t new_size = ht->base_size / 2;
    ht_resize(ht, new_size);
}

static size_t ht_get_hash(const char *s, size_t num_buckets, size_t attempt) {
    size_t hash_a = ht_hash(s, HT_PRIME_1, num_buckets);
    size_t hash_b = ht_hash(s, HT_PRIME_2, num_buckets);
    return (hash_a + (attempt * (hash_b + 1))) % num_buckets;
}

static size_t ht_hash(const char *str, size_t prime, size_t max) {
    size_t hash = 0;
    const size_t len_s = strlen(str);

    for (size_t i=0;i<len_s;i++) {
        size_t tmp = (size_t)pow((double)prime, (double)len_s - (double)i+1.0);
        hash += tmp * (unsigned char)str[i];
        hash = hash % max;
    }
    return hash;
}

static size_t is_prime(const size_t x) {
    if (x < 2) return 0;
    if (x < 4) return 1;
    if ((x % 2) == 0) return 0;

    for (size_t i=3; i<=floor(sqrt((double)x)); i+=2) {
        if ((x % i) == 0)
            return 0;
    }

    return 1;
}

static size_t next_prime(size_t x) {
    while (is_prime(x) != 1)
        x++;

    return x;
}
