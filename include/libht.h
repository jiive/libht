/* Copyright (C) Joakim Vinteräng 2020
 * https://gitlab.com/jovii/libht
 * License: GPLv3+, see LICENSE file for details */

#ifndef HASH_TABLE_H
#define HASH_TABLE_H

typedef struct {
    char *key;
    void *value;
} ht_item;

typedef struct {
    size_t base_size;
    size_t size;
    size_t count;
    ht_item **items;
} ht_hash_table;

#define HT_PRIME_1 1427
#define HT_PRIME_2 2311

ht_hash_table* ht_new();
void ht_insert(ht_hash_table *ht, const char *key, void *value);
void* ht_search(ht_hash_table *ht, const char *key);
void ht_delete(ht_hash_table *ht, const char *key);
void ht_del_hash_table(ht_hash_table *ht);

#endif
