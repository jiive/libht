LINK_TARGET=ht

VPATH=include/c-vector:include:lib

# Flags
CC=cc
CFLAGS=-g -Wall -Wextra -Wpedantic -Wconversion
LDFLAGS=-lm

# Directories
ODIR=./obj
SDIR=./src
LDIR=./lib
IDIR=./include

# Libraries
LIBS=

# Objects
# SRC = $(wildcard $(SDIR)/*.c)
SRC = $(shell find . -name *.c)
OBJ = $(patsubst $(SDIR)/%.c, $(ODIR)/%.o, $(SRC))

# Targets
all : $(LINK_TARGET)
	@echo "Done!"

$(LINK_TARGET): $(OBJ)
	@echo "Building target $@"
	LANG=C $(CC) $(CFLAGS) $(LDFLAGS) $(OBJ) -o $(LINK_TARGET)
	@echo .

$(ODIR)/%.o: $(SDIR)/%.c ctags
	@echo "Building dependency $@"
	LANG=C $(CC) $(CFLAGS) $(LDFLAGS) -c $< -o $@ -I$(IDIR)
	@echo .

# Dependencies
# main.o : inputstream.o

# Nice-to-haves
.PHONY: clean memcheck ctags

ctags:
	ctags -R .

clean:
	rm -f $(ODIR)/*.o $(INCDIR)/*~ $(SDIR)/*~ $(LINK_TARGET) *.core

memcheck:
	@echo "Running valgrind..."
	@valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all \
		-v --log-file=valgrind.log \
		./$(LINK_TARGET)
	@echo "Valgrind log written to valgrind.log"
